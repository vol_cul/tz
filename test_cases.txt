Тест-кейсы для проверки функционала добавить в избранное на странице.
id: AF-1.1.
  module: prom.ua, страница с категории “Велосипедные шины”, страница в виде галереи.
  title: Запуск, открыть страницу, навести мышку на блок с продуктом.
  environment: Google Chrome.
  steps:
        1. Открыть браузер указанный в environment.
        2. Перейти на страницу "http://prom.ua/Velosipednye-shiny?output=gallery".
        3. Навести курсор мышки на блок с продуктом на странице (к прим. "Покришка CST С1232 24х2,10 МТВ чорний")
        4. Навести курсор мышки на пиктограмму со звездочкой в вверхнем левом углу.
  expected_result:
        1. Появится поисковое окно.
        2. Откроется страница с продуктами в виде галереи.
        3. Даный блок выделится, появится пиктограмму со звездочкой в вверхнем левом углу, внизу блока появится доп. инф. 
        и в левом нижнем углу пиктограмма со звездочкой и текстом “Добавить в избранное”.
        4. Под ней появится окно с надписью "Добавить в избранное".

id: AF-1.2.
  module: prom.ua, страница с категории “Велосипедные шины”, страница в виде галереи.
  title: Запуск, открыть страницу, добавить товар в избранное.
  environment: Google Chrome.
  steps:
        1. Выполнить шаги описаные в пунктах № 1-4 согласно test-case c id -- AF-1.1.
        2. Нажать на пиктограмму со звездочкой в вверхнем левом углу.
  expected_result:
        1. Совпадает с пунктами описаными в пунктах № 1-3 согласно test-case c id -- AF-1.1.
        2. Пиктограмма со звездочкой меняет свой цвет, появляется подсказка о том, что товар добавлен в избранное
        текст “Добавить в избранное” изменяется на “Убрать из избранного”.
        3. Товар добавлен в избранное.

id: AF-1.3.
  module: prom.ua, страница с категории “Велосипедные шины”, страница в виде галереи.
  title: Запуск, открыть страницу, счетчик товаров добавленных в избранное.
  steps:
        1. Выполнить шаги описаные в пунктах № 1-3 согласно test-case c id -- AF-1.1.
        2. Нажать на пиктограмму со звездочкой в вверхнем левом углу.
  expected_result:
          1. Совпадает с пунктами описаными в пунктах № 1-3 согласно test-case c id -- AF-1.1.
          2. Пиктограмма со звездочкой меняет свой цвет, появляется подсказка о том, что товар добавлен в избранное
          текст “Добавить в избранное” изменяется на “Убрать из избранного”.
          3. Появится счетчик добавленных в избранное товаров, указывающий кол-во добавленных товаров.

id: AF-1.4.
  module: prom.ua, страница с категории “Велосипедные шины”, страница в виде галереи.
  title: Запуск, открыть страницу, добавленный товар появился на страница со списком всех добавленных в избранное товаров.
  steps:
        1. Выполнить шаги описаные в пунктах № 1-4 согласно test-case c id -- AF-1.1.
        2. Нажать на пиктограмму со звездочкой в вверхнем левом углу.
        3. Перейти на страницу "http://prom.ua/comparison/list".
  expected_result:
          1. Совпадает с пунктами описаными в пунктах № 1-4 согласно test-case c id -- AF-1.1.
          2. Пиктограмма со звездочкой меняет свой цвет, появляется подсказка о том, что товар добавлен в избранное
          текст “Добавить в избранное” изменяется на “Убрать из избранного”.
          3. Появится счетчик добавленных в избранное товаров, указывающий кол-во добавленных товаров.
          4. При переходе на указанную страницу, появится список всех добавленных в избранное товаров.
          Он должен совпадать с тем товаром, который был добавлен.

id: AF-1.5.
  module: prom.ua, страница с категории “Велосипедные шины”, страница в виде галереи.
  title: Запуск, открыть страницу, добавленный товар появился на страница со списком всех добавленных в избранное товаров.
  steps:
        1. Выполнить шаги описаные в пунктах № 1-3 согласно test-case c id -- AF-1.4.
        2. Нажать на пиктограмму со звездочкой в вверхнем левом углу.
        3. Перейти на страницу "http://prom.ua/comparison/list".
        4. Навести курсор на крестик в блоке с выбранным товаром.
        5. Нажать на него.
  expected_result:
          1. Совпадает с пунктами описаными в пунктах № 1-4 согласно test-case c id -- AF-1.4.
          2. Крестик поменяет свой стиль.
          3. Товар удалится из списка добавленных в избранное.
          4. Счетчик товаров обнулится.

