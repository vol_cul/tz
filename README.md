# README #

This is test task for [evo.company](https://evo.company/).

To run in yours machine, make some step:
1. Create folder for project.
```
#!bash

mkdir project
```
2. Go there.
```
#!bash

cd path_to_created_folder/project
```
3. Clone this repo there. 
```
#!bash

git clone https://vol_cul@bitbucket.org/vol_cul/tz.git
```

4. Make sure that you have added the path of the chromedriver file to the PATH shell variable.
5. Go to *test/* folder 
```
#!bash

cd tz/test
```
6. Run. 
```
#!bash

python -m unittest -v test
```