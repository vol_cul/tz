# -*- coding: utf-8 -*-

import unittest
import sys
from datetime import datetime


from selenium.webdriver import Chrome
from selenium.webdriver import ActionChains
from core.pagewithgallery import PageWithGallery


class TestCasePromUa(unittest.TestCase):

	def setUp(self):
		self.url = 'http://prom.ua/Velosipednye-shiny?output=gallery'
		self.driver = Chrome()
		self.driver.get(self.url)
		self.action = ActionChains
		self.test_page = PageWithGallery(self.driver)

	def test_add_prod_to_fav(self):
		self.test_page.add_product_to_fav(self.action)
		self.assertEqual(u'1', self.test_page.get_counter())

	def test_of_visibility(self):
		self.test_page.hover_product_block(self.action)
		self.assertIs(self.test_page.star_element.is_displayed(), True)

	def test_is_star_active(self):
		self.test_page.add_product_to_fav(self.action)
		self.assertIs(self.test_page.is_star_active(), True)

	def test_string_has_changed(self):
		self.test_page.add_product_to_fav(self.action)
		self.assertEqual(
			u'Убрать из избранного',
			self.test_page.comparision_link_element.text,
		 )

	def test_prod_in_fav_list(self):
		self.test_page.add_product_to_fav(self.action)
		prod_id = self.test_page.get_product_block_id()
		self.test_page.go_to_page_with_list_of_added_product(self.action)
		self.assertEqual(self.test_page.get_product_block_id(), prod_id)

	def test_del_prod_from_fav(self):
		self.test_page.add_product_to_fav(self.action)
		self.test_page.go_to_page_with_list_of_added_product(self.action)
		self.test_page.remove_prod_from_chosen(self.action)
		self.assertEqual(u'0', self.test_page.get_count_of_favorite_product())


	def tearDown(self):
		is_test_successfull = sys.exc_info()[0]
		if not is_test_successfull:
			test_method_name = self._testMethodName
			date = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
			self.driver.save_screenshot(
				'{}/{}/{}.png'.format(self.url, test_method_name, date),
			)
		super(TestCasePromUa, self).tearDown()
		self.driver.quit()

if __name__ == '__main__':
	unittest.main()
