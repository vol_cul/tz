from selenium.webdriver.support.ui import WebDriverWait
from core.locators import MainPageLocators


class BaseElement(object):
    def __get__(self, obj, owner):
        """Gets the text of the specified object"""
        driver = obj.driver
        WebDriverWait(driver, 200).until(
            lambda driver: driver.find_element(*self.locator))
        element = driver.find_element(*self.locator)
        return element


class ProductBlockElement(BaseElement):
    locator = MainPageLocators.PROD_BLOCK


class StarElement(BaseElement):
    locator = MainPageLocators.COMPARISON_STAR


class ComparisionLinkElement(BaseElement):
    locator = MainPageLocators.COMPARISON_LINK

class CounterOfFavoriteProduct(BaseElement):
    locator = MainPageLocators.COUNTER_OF_ITEMS
