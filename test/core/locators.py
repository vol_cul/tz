from selenium.webdriver.common.by import By


class MainPageLocators(object):

    PROD_BLOCK = (
        By.CSS_SELECTOR, '[data-qaid="product-block"]',
    )
    COMPARISON_STAR = (
        By.CSS_SELECTOR,
        '[data-qaid="product-block"]:first-child [data-qaid="comparison-star"]',
    )
    COMPARISON_LINK = (
        By.CSS_SELECTOR,
        '[data-qaid="product-block"]:first-child [data-qaid="comparison-link"]',
     )
    LINK_COUNTER = (
        By.CSS_SELECTOR,
        '[data-qaid="header-block"] [data-qaid="comparison-star"] + *',
    )
    CATEGORY_TAB = (By.CSS_SELECTOR, '[data-qaid="category-tab"]')
    REMOVE_BUTTON = (By.CSS_SELECTOR, '[data-qaid="remove-btn-from-comparison"]')
    COUNTER_OF_ITEMS = (
        By.CSS_SELECTOR,
        '[data-qaid="category-tab"]:first-child span',
    )
