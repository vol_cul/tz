from selenium.webdriver.support.ui import WebDriverWait
from core.locators import MainPageLocators
from core.basepage import BasePage
from core.elements import (
	ProductBlockElement,
	StarElement,
	ComparisionLinkElement,
	CounterOfFavoriteProduct,
)


class PageWithGallery(BasePage):

	product_block = ProductBlockElement()
	star_element = StarElement()
	comparision_link_element = ComparisionLinkElement()
	counter_of_favorite_product = CounterOfFavoriteProduct()

	def _elem_on_block_and_click(self, action, element_on_block):
		act = action(self.driver)
		act.move_to_element(self.product_block).perform()
		element_on_block = self.driver.find_element(*element_on_block)
		act.move_to_element(element_on_block).click(element_on_block).perform()
		return element_on_block

	def get_counter(self):
		element = self.driver.find_element(*MainPageLocators.LINK_COUNTER)
		return element.get_attribute('innerHTML')

	def hover_product_block(self, action):
		act = action(self.driver)
		act.move_to_element(self.product_block).perform()

	def add_product_to_fav(self, action):
		self._elem_on_block_and_click(action, MainPageLocators.COMPARISON_STAR)

	def get_product_block_id(self):
		return self.product_block.get_attribute('data-ad_product_id')

	def is_star_active(self):
		return 'icon-comparison-active' in self.star_element.get_attribute('class')

	def go_to_page_with_list_of_added_product(self, action):
		element = self.driver.find_element(*MainPageLocators.LINK_COUNTER)
		act = action(self.driver)
		act.move_to_element(element).perform()
		act.click().perform()


	def remove_prod_from_chosen(self, action):
		remove_button = WebDriverWait(self.driver, 100).until(
			lambda driver: driver.find_element(*MainPageLocators.REMOVE_BUTTON))
		act = action(self.driver)
		act.move_to_element(remove_button).perform()
		act.click().perform()

	def  get_count_of_favorite_product(self):
		return self.counter_of_favorite_product.text


